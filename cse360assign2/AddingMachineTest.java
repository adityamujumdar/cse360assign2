/*
 * Author: Aditya Mujumdar 
 * Class ID: 70642 (W 9:40am to 10:30am)
 * Assignment 2
 * Description: This file contains the required Junit tests for
 * testing the functions of AddingMachine.java
 */

package cse360assign2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class AddingMachineTest {

	/*
	 * tests if the getTotal() method sets the total to 0
	 */
	@Test
	public void testGetTotal() {
		AddingMachine testMachine = new AddingMachine();
		assertEquals(0,testMachine.getTotal());
	}
	/*
	 * tests the functioning of add() method
	 */
	@Test
	public void testAdd() {
		AddingMachine testMachine = new AddingMachine();
		testMachine.add(3);
		testMachine.add(7);
		assertEquals(10,testMachine.getTotal());
	}
	/*
	 * tests the functioning of subtract() method
	 */
	@Test
	public void testSubtract() {
		AddingMachine testMachine = new AddingMachine();
		testMachine.add(7);
		testMachine.subtract(3);
		assertEquals(4, testMachine.getTotal());
	}
	/*
	 * tests returning the saved history of all calculations
	 */
	@Test
	public void testHistory() {
		AddingMachine testMachine = new AddingMachine();
		testMachine.add(4);
		testMachine.subtract(2);
		testMachine.add(5);
		assertEquals("0 + 4 - 2 + 5", testMachine.toString());
	}
	
	/*
	 * tests clearing the recorded calculation history
	 */
	@Test
	public void testClearHistory() {
		AddingMachine testMachine = new AddingMachine();
		testMachine.add(4);
		testMachine.subtract(2);
		testMachine.add(5);
		testMachine.add(9);
		testMachine.clear();
		assertEquals("0", testMachine.toString());
	}
	
	/*
	 * tests clearing the total parameter after performing add and subtract operations
	 */
	@Test
	public void testClearTotal() {
		AddingMachine testMachine = new AddingMachine();
		testMachine.add(4);
		testMachine.subtract(2);
		testMachine.add(5);
		testMachine.add(9);
		testMachine.clear();
		assertEquals(0, testMachine.getTotal());
	}
}
