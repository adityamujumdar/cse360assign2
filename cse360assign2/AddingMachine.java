/*
 * Author: Aditya Mujumdar 
 * Class ID: 70642 (W 9:40am to 10:30am)
 * Assignment 2
 * Description: This assignment makes a AddingMachine class which adds, subtracts,
 * and prints the values and operations performed. This assignment is geared towards
 * getting familiar with the workings of version control systems.
 */

package cse360assign2;

/*
 * This class adds, subtracts, prints values of performed operations.
 * @author Aditya Mujumdar
 */
public class AddingMachine {

	/*
	 * Represents the total value
	 */
	private int total;
	/*
	 * Records the history of calculations
	 */
	private String h_tracker = "0";
	
	/*
	 * Constructor helps set total to 0
	 */
	public AddingMachine () {
		total = 0;  // not needed - included for clarity
	}
	
	/*
	 * Returns the current total value
	 * @return an integer which represents total value
	 */
	public int getTotal () {
		return total;
	}
	
	/*
	 * Adds the parameter to total variable
	 * @param value the value to be added to total
	 */
	public void add (int value) {
		total = total + value;
		// track calculation history for adding 'value'
		h_tracker = h_tracker.concat(" + " + value);
	}
	
	/*
	 * Subtracts the parameter from total variable
	 * @param value the value to be subtracted from total
	 */
	public void subtract (int value) {
		total = total - value;
		// track calculation history for subtracting 'value'
		h_tracker = h_tracker.concat(" - " + value);
	}
	
	/*
	 * keeps a history of transactions made
	 * @return a String with history of all transactions
	 */	
	public String toString () {		
		return h_tracker;
	}
	
	/*
	 * clears the toString history
	 */
	public void clear() {
		total = 0;
		h_tracker = "0";
	}
}
